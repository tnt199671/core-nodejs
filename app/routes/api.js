//middleware
import authentication from "../middleware/authentication";
//permission
import permis from "../middleware/permission";
//controller
import testController from "../controllers/testController";
import loginController from "../controllers/loginController";

export default (app)=>{
	app.get("/", (req, res)=>
		res.status(200).json({"key": "welcome to app"})
	);
	app.group('/api/v1',(router)=>{
		router.get('/login', loginController.login);
		router.use([authentication]);
		//url ---> routeName ---> function
		router.get("/get-data",permis('ahihi'), testController.getData);
		router.post("/add-data", testController.addData);
	});
	
}