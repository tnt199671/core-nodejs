export default class test {
	static async getDataGPS() {
		const data = await DB.select('*').order_by(['car_id ASC', 'gps_time DESC']).limit(20).offset(0).get('gps');
		return data;
	}
}