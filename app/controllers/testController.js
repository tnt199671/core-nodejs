import test from "../model/test";
import controller from "./controller";
export default class recipeController extends controller 
{

    static async getData(request, response) {
        try {
            const data = await test.getDataGPS();
            return super.responseSuccess(response,data);
        }catch(error) {
            return super.responseOther(response,500,error);
        }
    }
    // add a new recipe with unique id
    static addData(request, response) {
        const newId = parseInt(data.length) + 1;
        const { name, description } = request.body;
        const newRecipe = {
            id: newId,
            name,
            description,
            addedAt: new Date()
        };
        data.push(newRecipe);
        return response.status(200).json({
            message: "A new recipe has been added",
            data: {id: newId, name: name}
        });
    }
}