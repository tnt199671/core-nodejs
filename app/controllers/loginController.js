import controller from "./controller";
import jwt from "jsonwebtoken";
import user from "../model/user";
export default class loginController extends controller 
{
    static async login(request, response, next) {
        try {
            const data = await user.getUserLogin(request.body.userName);
            const token = jwt.sign(JSON.stringify(data[0]), global.env.JWT_SECRET_KEY);
            return super.responseSuccess(response,{
                token:token
            });
        }catch(error) {
            return super.responseOther(response,500,error);
        }
    }
}