export default class controller {
    static responseSuccess(response, data = [], message = '') {
        DB.release();
        return response.status(200).json({
            success: true,
            code: 200,
            data: data,
            message: message,
            errors: []
        });
    }

    static responseFail(response, errors = [], message = '') {
        DB.release();
        return response.status(200).json({
            success: false,
            code: 400,
            data: [],
            message: message,
            errors: errors
        });
    }

    static responseOther(response, code = 200, message = '') {
        DB.release();
        return response.status(200).json({
            success: false,
            code: code,
            data: [],
            message: message,
            errors: []
        });
    }
}