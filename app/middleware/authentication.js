import jwt from "jsonwebtoken";
class authentication {
	static async handle(request,response,next) {
		let authorization = request.headers.authorization;
		if (authorization && authorization.split(' ')[0] === 'Bearer') {
			let token = authorization.split(' ')[1];
			jwt.verify(token, global.env.JWT_SECRET_KEY, function(error, decode) {
				if(error){
					return response.status(200).json({
			            success: false,
			            code: 401,
			            data: [],
			            message: 'Unauthorized',
			            errors: []
			        });
				}
				request.user = decode;
				next();
			});
		} else{
			return response.status(200).json({
	            success: false,
	            code: 403,
	            data: [],
	            message: 'Token is Invalid',
	            errors: []
	        });
		}
	}
}
export default authentication.handle;