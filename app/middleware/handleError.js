class handleError {
    static handle(error,request, response, next)
	{
		if(error){
			DB.release();
			return response.status(200).json({
	            success: false,
	            code: 500,
	            data: [],
	            message: error.toString(),
	            errors: []
	        });
		}else{
			next();
		}
	}
};

export default handleError.handle

