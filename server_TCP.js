import net from 'net';
import Parser from 'teltonika-parser';
import binutils from 'binutils64';
import moment from 'moment';
import dotenv from 'dotenv';
import QueryBuilder from 'node-querybuilder';
const env = dotenv.config().parsed;
const settings = ({
    database: env.DATABASE,
    host: env.DB_HOST,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
});
const pool = new QueryBuilder(settings, 'mysql', 'pool');

class server
{
	static async handle() {
		let context = this;
		var server = net.createServer(); 
		server.listen(8160, '0.0.0.0');
		server.on('connection', function(socket) {
			let imei = '';
			socket.on('data', async function (data) {
				try{
					let buffer = data;
					let parser = new Parser(buffer);
					if(parser.isImei){
						socket.write(Buffer.alloc(1, 1));
						if(!imei){
							imei = await context.getInforDevice(data.toString());
						}
					}else {
						let avl = parser.getAvl();
						context.saveData(avl,imei);
						let writer = new binutils.BinaryWriter();
						writer.WriteInt32(avl.number_of_data);

						let response = writer.ByteBuffer;  
						socket.write(response);
						socket.destroy();
					}
				}catch(error){console.log(error);
					socket.destroy();
				}
			});
		});
		server.on('end', function(socket) {
			
		});
	}

	static async saveData(data,imei){
		for (let [key, value] of Object.entries(data.records)) {
			let gps = value.gps;
			let record = {
				imei_id: imei.id,
				car_id: imei.car_id,
				org_id: imei.org_id,
				gps_time: moment(value.timestamp).format("YYYY-MM-DD HH:mm:ss"),
				longitude: gps.longitude,
				latitude: gps.latitude,
				altitude: gps.altitude,
				angle: gps.angle,
				satellites: gps.satellites,
				speed: gps.speed
			};
			let ioElements = await this.getDataIoElements(value.ioElements);
			record = Object.assign(record, ioElements);
			await this.insertData(record);
		}
	}

	static async getDataIoElements(data){
		let record = {};
		for (let [key, value] of Object.entries(data)) {
			switch(value.id) {
				case 239:
    				record.ignition = value.value;
    			break;
    			case 240:
    				record.movement = value.value;
    			break;
    			case 21:
    				record.GSM_signal = value.value;
    			break;
    			case 200:
    				record.sleep_mode = value.value;
    			break;
    			case 69:
    				record.GNSS_status = value.value;
    			break;
    			case 181:
    				record.GNSS_PDOP = value.value;
    			break;
    			case 182:
    				record.GNSS_HDOP = value.value;
    			break;
    			case 66:
    				record.external_voltage = value.value;
    			break;
    			case 67:
    				record.battery_voltage = value.value;
    			break;
    			case 68:
    				record.battery_current = value.value;
    			break;
    			case 241:
    				record.active_GSM_operator = value.value;
    			break;
    			case 16:
    				record.total_odometer = value.value;
    			break;
    			default:
    			break;
    		}
    	}
    	return record;
    }

    static async getInforDevice(imei){
    	const DB = await pool.get_connection();
    	imei = imei.replace('\u0000\u000f','');
    	let data = await DB.select('*').where('imei',imei).get('imei');
    	DB.release();
		return data[0];
    }

    static async insertData(data){
    	const DB = await pool.get_connection();
    	DB.returning('id').insert('gps', data);
    	DB.release();
    }
}

export default server.handle();