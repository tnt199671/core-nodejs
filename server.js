//import
import express from 'express';
import routes from './app/routes/api';
import 'express-group-routes';
import dotenv from 'dotenv';
import QueryBuilder from 'node-querybuilder';
import handleError from "./app/middleware/handleError";
import './server_TCP';
// import socket from './server_socket';
class server
{
    static async handle() {
        const app = express();
        app.use(express.json());
        // app.set('views', './app/views');
        // app.set('view engine', 'ejs');

        //env
        const env = dotenv.config().parsed;
        global.env = env;

        const settings = ({
            database: env.DATABASE,
            host: env.DB_HOST,
            user: env.DB_USER,
            password: env.DB_PASSWORD,
        });
        const pool = new QueryBuilder(settings, 'mysql', 'pool');

        //config CORS
        app.use(async function(request, response, next) {
            global.DB = await pool.get_connection();
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        //routes
        routes(app);
        //handleError
        app.use(handleError);
        app.use( (request, response, next)=>{
            response.status(404);
            if (request.accepts('json')) {
                response.send({
                    success: true,
                    code: 404,
                    data: [],
                    message: 'Đường dẫn không hợp lệ!',
                    errors: []
                });
                return;
            }
        });
        app.listen(env.NODE_PORT, ()=>{
          console.log('app is listening on port 3000');
        }); //open port
    }
}

server.handle();
