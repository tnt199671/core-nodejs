var httpdispatcher = require('httpdispatcher');
var axios = require('axios');
var uuid = require("node-uuid");
var dispatcher = new httpdispatcher();
var server = require('http').createServer(handleRequest);
server.listen(global.env.SERVER_PORT);
var io = require('socket.io').listen(server);
global.io = io;
var env = global.env;
var listRoom = new Map();

function handleRequest(request, response){
	try {
        // log the request on console
        console.log(123);
        // Dispatch
        dispatcher.dispatch(request, response);
    } catch(err) {
    	console.log(err);
    }
}

dispatcher.onGet("/server", function(req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.end('<h1>Hey, this is the homepage of your server</h1>');
});